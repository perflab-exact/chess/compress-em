import os
import sys

# usage: python compressai_test.py <image_path>
# modify the path to compressai accordingly

compressai = "/home/bozhan/repo/compressai/examples/codec.py"

model_list = ["bmshj2018-factorized",
              "bmshj2018-factorized-relu",
              "bmshj2018-hyperprior",
              "mbt2018-mean",
              "mbt2018",
              "cheng2020-anchor",
              "cheng2020-attn"
              ]

model = "bmshj2018-factorized"

input_path = sys.argv[1]

end_pos = input_path.rfind(".")
file_name = input_path[:end_pos]

os.system(f"python -u {compressai} encode {input_path} --model {model} -q 1 --cuda -o {file_name}_1.compressai")
os.system(f"python -u {compressai} decode {file_name}_1.compressai --cuda -o {file_name}_compressai_decomp.png")