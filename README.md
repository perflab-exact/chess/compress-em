# MSCompression Repository README

Welcome to the MSCompression repository, a cutting-edge solution for High performance AI-based image compression. This README provides essential information on how to set up and run the MSCompression project on your system.

## Prerequisites

Before you begin, ensure you have the following installed:

- Python 3.x
- CUDA (12.x)
- Pip (Python package installer)
- Git (optional, for cloning the repository)

## Installation

Follow these steps to set up the MSCompression environment on your machine:

1. **Install CompressAI:**
   CompressAI is a required dependency for MSCompression. Install it using pip by running the following command in your terminal:

   ```bash
   pip install compressai
   ```

2. **Clone/Download the MSCompression Repository:**
   If you have Git installed, you can clone the repository using the following command:

   ```bash
   git clone [URL_OF_MSCompression_REPOSITORY]
   ```

   Alternatively, you can download the repository as a ZIP file and extract it.

3. **Navigate to the GPULZ Directory:**
   Change your current directory to the `gpulz` directory within the cloned or downloaded repository:

   ```bash
   cd gpulz
   ```

4. **Compile the GPULZ Project:**
   Compile the GPULZ project using the `make` command. The `-j` flag is used to speed up the compilation process by running multiple jobs in parallel:

   ```bash
   make -j
   ```

## Running the Example

Once you have successfully installed and compiled GPULZ, you can run the provided example:

1. **Open the `pipeline.ipynb` Notebook:**
   Locate and open the `pipeline.ipynb` Jupyter Notebook. This notebook contains the example pipeline for using MSCompression.

2. **Run the Notebook:**
   Execute the cells in the notebook sequentially. These cells demonstrate how to use MSCompression with the provided example.

3. **Example Data:**
   An example image is already included in the `dataset` folder of the repository. The notebook will use this image to showcase the capabilities of MSCompression.

## Support

If you encounter any issues or have questions, please feel free to open an issue in the repository's issue tracker.

Thank you for choosing MSCompression for your GPU-accelerated processing needs!