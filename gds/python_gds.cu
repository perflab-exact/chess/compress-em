#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <fstream>
#include <chrono>

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cuda_runtime.h>
#include "cufile.h"

using namespace std;

void gdsRead(char* testfn, void *devicePrt, size_t fileSize)
{
    int fileDescriptor;
    ssize_t ret;

    off_t fileOffset = 0;
    off_t devicePtrOffset = 0;

    CUfileError_t status;
    CUfileDescr_t cfDescriptor;
    CUfileHandle_t cfHandle;

    fileDescriptor = open(testfn, O_RDONLY | O_DIRECT);
    if (fileDescriptor < 0)
    {
        std::cerr << "file open " << testfn << "errno " << errno << std::endl;
        return;
    }

    status = cuFileDriverOpen();
    if (status.err != CU_FILE_SUCCESS)
    {
        std::cerr << " cuFile driver failed to open " << std::endl;
        close(fileDescriptor);
        return;
    }
    return;

    memset((void *)&cfDescriptor, 0, sizeof(CUfileDescr_t));
    cfDescriptor.handle.fd = fileDescriptor;
    cfDescriptor.type = CU_FILE_HANDLE_TYPE_OPAQUE_FD;
    status = cuFileHandleRegister(&cfHandle, &cfDescriptor);
    if (status.err != CU_FILE_SUCCESS)
    {
        std::cerr << "cuFileHandleRegister fileDescriptor " << fileDescriptor << " status " << status.err << std::endl;
        close(fileDescriptor);
        return;
    }

    auto start = std::chrono::high_resolution_clock::now();
    ret = cuFileRead(cfHandle, devicePrt, fileSize, fileOffset, devicePtrOffset);
    auto stop = std::chrono::high_resolution_clock::now();

    if (ret < 0)
    {
        std::cerr << "cuFileWrite failed " << ret << std::endl;
    }

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

    std::cout << "GDS read time: " << duration.count() << " ms" << std::endl;
    // std::cout << "GDS bandwidth: " << (float(fileSize) / float(duration.count())) << " GB/s" << std::endl;

    // cudaFree(devicePrt);
    (void)cuFileHandleDeregister(cfHandle);
    close(fileDescriptor);
    (void)cuFileDriverClose();
    return;
}

extern "C"
{
    void python_gdsRead(char* testfn, void *devicePrt, size_t fileSize)
    {
        gdsRead(testfn, devicePrt, fileSize);
    }
}