import os
import sys

input_path = sys.argv[1]
x = int(sys.argv[2])
y = int(sys.argv[3])

a_model = "/home/bozhan/repo/mscompression_official/test/bmshj2018_factorized_a.pt"
s_model = "/home/bozhan/repo/mscompression_official/test/bmshj2018_factorized_s.pt"

os.system(f"./mscomp -c -m {a_model} -i {input_path} -o {input_path[:-4]}.comp -x {x} -y {y}")
os.system(f"./mscomp -d -m {s_model} -i {input_path[:-4]}.comp -o {input_path[:-4]}.decomp -x {x} -y {y}")